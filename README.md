##########################################
## Instalación de OML-Dialer en Centos7 ##
##########################################

Para OMLDialer también se desarrolló un playbook de ansible con el fin de instalarlo. El siguiente paso a pasó está basado en un Centos7 minimal installation usando el esquema Ansible-Deployer VS CentOS7-App-Host).

Prerequisitos en la VM App-Host
-------------------------------

* Crear el usuario ftsender y asignarle una contraseña.

```
useradd ftsender
passwd ftsender
```

* Editar el archivo sudoers con visudo y agregar la siguiente línea

```
visudo
```

```
ftsender ALL=(ALL) NOPASSWD: ALL
```


Prerequisitos en el deployer
-----------------------------

Para disponer de un deployer, se necesita contar con ansible instalado. 

* Instalar pip3, git y ansible

```
 apt-get install python3-pip git -y
 pip3 install 'ansible==2.9.2'

```

* Clonar el repositorio de ftsenderweb con el  usuario root

```    
  git clone https://gitlab.com/omnileads/omnileads-dialer.git
  cd omnileads-dialer
  cd deploy
```

* Editar el inventory con la IP del servidor donde se quiere instalar OMLDialer y el puerto SSH para realizar la conexión remota

```
  [ics-centos]
  192.168.95.20 ansible_ssh_port=22
```    

* Ejecutar el instalador. Es demasiado similar al instalador de Omnileads
    
```
 sudo ./deploy-ics.sh -i
```

Recuerde tener a mano la contraseña que asignó al usuario ftsender creado en el target host.

Creación del superuser
----------------------

Una vez instalado es necesario crear el superusuario, que será el admin de la plataforma, para eso es necesario ingresar por SSH al servidor y ejecutar el siguiente script como root.

```
cd /home/ftsender/deploy/bin/
./manage.sh createsuperuser    
```

Pedirá un nombre de usuario, email (se puede dejar vacio) y contraseña. Luego de creado el usuario se puede ingresar a OMLDialer via web:

```
http://IP_SERVER:8088
```

Configuración de la troncal a discar por OMLDialer
--------------------------------------------------

OMLDialer debe ser troncalizado con el Asterisk de OMniLeads (ya sea AIO o desplegado sobre un host aislado), para ello generamos la siguiente configuración sobre el archivo *sip_custom.conf*:

```
[omnileads]
host=your_omlasterisk_netaddr
disallow=all
allow=alaw
type=peer
secret=my_very_strong_pass
defaultuser=dialer
fromuser=dialer
port=5161
dtmfmode=rfc2833
qualify=yes
insecure=invite
context=from-internal
callerid=OML Dialer
```

El correspondiente TRUNK en OML es:

* Nombre del Trunk

```
dialer
```

* Configuracion

```
type=wizard
transport=trunk-transport
accepts_registrations=no
sends_auth=no
sends_registrations=no
accepts_auth=yes
endpoint/rtp_symmetric=yes
endpoint/force_rport=yes
endpoint/rewrite_contact=yes
endpoint/timers=yes
aor/qualify_frequency=60
aor/authenticate_qualify = no
endpoint/allow=alaw,ulaw
endpoint/dtmf_mode=rfc4733
endpoint/context=from-dialer
inbound_auth/username=dialer
inbound_auth/password=my_very_strong_pass
```
