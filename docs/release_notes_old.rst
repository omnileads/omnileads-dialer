Release Notes previas
=====================

Sprint 5 - 25 de junio de 2014
------------------------------

Nueva funcionalidad
...................

* Reciclado de campaña y base de datos de contactos
* Supervisión: mostrar estado actual del Daemon
* Derivación de llamadas
* Refrescar automaitcamente pantalla de supervisión
* Listado de contactos q' presionaron una opción (en vivo, estilo supervisión)


Deploy
......

* Deploy automatico de configuracion de PostgreSql
* Deploy de procedimientos almacenados plpythonu
* Armado de ambientes de testing y de deploy

Bugs solucionados
.................

* Alta de campaña: error 500 si conversion de audio falla


Sprint 6 - 11 de julio de 2014
------------------------------

Nueva funcionalidad
...................

* Implementacion de Celery para ejecución de tareas asincronas / en background
* Finalización manual de campañas en background
* FTS-212 - Nuevas formas de reciclado de campaña (Ocupados/No contestó/Número erróneo/Llamada errónea)
* FTS-234 - Validación consistencia de fechas y actuaciones de campañas
* FTS-231 - Daemon evita finalizar campañas que posean llamadas en curso
* FTS-152 - Depuración de eventos de contactos
* FTS-205 - Depuración de BD de contactos
* FTS-208 - Mejora UI de carga de audio de campaña


Deploy
......

* Deploy de Redis
* Deploy de workers Celery


Bugs solucionados
.................

* FTS-169 - Reporta problemas con reload de configuracion de Asterisk
* FTS-206 - Valida y acepta csv con una sola columna (sin delimitador)


Otras mejoras
.............

* Unificación de archivos de dependencias y librerías Python
* Ajustes a instrucciones de deploy


Sprint 7 - 25 de julio de 2014
------------------------------

Instrucciones de deploy
.......................

.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint7-fixes <INVENTARIO>

Nueva funcionalidad
...................

* FTS-13 - Eliminar Grupos de Atención
* FTS-14 - Modificar Grupos de Atención
* FTS-173 - Procesos largos deben mostrar progreso o mensaje
* FTS-211 - Agregar 'date picker' para fechas + widget para horarios
* FTS-240 - Permitir acceso a todos los tabs al crear y reciclar campaña
* FTS-246 - Reciclado de campañas, filtrando por más de un estado
* FTS-248 - Implementación de estado DEPURADA para Campaña, evita
  problemas de acceso concurrente e inconsistencias desde que la campaña
  es finalizada hasta que es depurada.
* FTS-252 - Mostrar en web la versión de la aplicación
* FTS-254 - Usar colores de FreeTech en UI
* FTS-259 - Borrar campaña
* FTS-260 - Identificar usuarios del sistema

Deploy
......

* Generacion de ambiente de desarrollo usando Docker

Bugs solucionados
.................

* FTS-249 - FIX: paths concatenados, en vez de generados con os.path.join()
* FTS-250 - FIX: FTS_BASE_DATO_CONTACTO_DUMP_PATH requiere finalizar con /
* FTS-255 - FIX "error interno" al intentar depurar BD de Contactos
* FTS-261 - FIX: script de inicio no hace reload desde scripts de deploy
* FTS-262 - FIX: script de deploy no deploya version más reciente



Sprint 8 - 4 de agosto de 2014
------------------------------

Instrucciones de deploy
.......................

.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint8-fixes <INVENTARIO>

Nueva funcionalidad
...................

* FTS-268 (y otras) - Derivación externa de llamadas
* FTS-279 - Crea comando para crear usuario para sistema web

Deploy
......

* FTS-274 - Hacer configurables limites de originates y otras settings del sistema
* FTS-267 - Implementa Supervisor para workers Celery
* FTS-199 - Separar distintos daemons

Bugs solucionados
.................

* FTS-207 - Reporte: unificar opciones invalidas en pie chart
* FTS-42 - Separar frameworks (bootstrap) de las customizaciones


Sprint 9 - 20 de agosto de 2014
-------------------------------


Instrucciones de deploy
.......................


.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint9-fixes <INVENTARIO>


Nueva funcionalidad: Templates de Campañas
..........................................

* FTS-285 FTS-286 - Creacion de templates de campañas
* FTS-287 FTS-289 - Creación de campañas desde templates de campaña
* FTS-288 - Listado y borrado de templates de campañas
* FTS-290 - Daemon: ignorar templates al buscar campañas a ejecutar

Nueva funcionalidad: Audios predefinidos
........................................

* FTS-291 FTS-292 - Creación de audios predefinidos
* FTS-292 - ABM y listado de audios predefinidos
* FTS-293 - Update de campañas y templates de campañas: permite seleccion
  de audios predefinidos
* FTS-294 - Conversión de formato de archivos de audios para audios predefinidos


Sprint 10 - 2 de septiembre de 2014 - 15 de septiembre de 2014
--------------------------------------------------------------


Instrucciones de deploy
.......................


.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint10-fixes <INVENTARIO>

Para crear usuarios, es necesario loguearse en el servidor con el usuario `ftsender`
y ejecutar `/home/ftsender/deploy/bin/manage.sh create_ftsender_user`:

.. code::

    $ host> ssh ftsender@server-or-ip
    $ server> /home/ftsender/deploy/bin/manage.sh create_ftsender_user


Nueva funcionalidad: importacion de campos extras de CSV
........................................................

* FTS-296 - Modifica BD/clases para soportar guardado de todos los campos del CSV
* FTS-298 - FTS-299 - Importador de BD desde CSV guarda todos los campos del CSV
* FTS-300 - Daemon: toma nros. telefonicos de la nueva estructura de datos
* FTS-301 - Exportador reportes CSV: exporta todos los datos importados del CSV
* FTS-302 - UI: creación de BD: adaptación a nueva estructura de datos
* FTS-303 - UI: listado de números que seleccionaron opciones: adaptación a nueva estructura de datos
* FTS-313 -	Refactorizar reciclado de BD: adaptación a nueva estructura de datos

Nueva funcionalidad: importación de campos fecha/hora
.....................................................

* FTS-304 -	Importador de BD: agrega funcionalidad para soportar campos fechas/hora
* FTS-305 -	UI: Creación/importación de BD: permite seleccionar tipos de datos de las columans

Diferidos para próximo Sprint
.............................

* FTS-312 - Soportar PostgreSql 8
* FTS-314 - Arreglar UI de "Listado de contactos que seleccionaron opcion"
* FTS-316 - Cancelar importacion de BD de contactos si validacion falla 


Sprint 11 - 2 de septiembre de 2014 - 15 de septiembre de 2014
--------------------------------------------------------------


Instrucciones de deploy
.......................


.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint11-fixes <INVENTARIO>

Para crear usuarios, es necesario loguearse en el servidor con el usuario `ftsender`
y ejecutar `/home/ftsender/deploy/bin/manage.sh create_ftsender_user`:

.. code::

    $ host> ssh ftsender@server-or-ip
    $ server> /home/ftsender/deploy/bin/manage.sh create_ftsender_user


Migraciones de datos
....................

En este sprint no se registraron migraciones de datos.


BUGs arreglados
...............

* FTS-319 - Sistema permite modificar objetos (Campañas, Bases de Datos
  de Contactos, etc) que no deberian poder modificarse
* FTS-320 - Bugs encontrados al implementar tests de las vistas


Bases de Datos de Contactos y Múltiples Audios
..............................................

* FTS-308 - Refactorizacion de modelos
* FTS-309 - UI: Alta de campaña (funcionalidad básica)
* FTS-314 - UI de "Listado de contactos que seleccionaron opcion"
* FTS-316 - Cancelar importacion de BD de contactos si validacion falla 
* FTS-318 - UI: Nombres de columnas de CSV: Definir nombres para las columnas


Diferidos para próximo Sprint
.............................

* FTS-312 - Soportar PostgreSql 8
* FTS-315 - ParserCsv abre file pero NO lo cierra
* FTS-311 - UI: Alta de campaña: volver a permitir modificación de BD
* FTS-307 - Campos fecha/hora: Generador de dialplan
* FTS-306 - Campos fecha/hora: Daemon: obtener metadatos de BD
* FTS-297 - Soporte para multiples sistemas de TTS


Sprint 12 - 16 de septiembre de 2014 - 29 de septiembre de 2014
---------------------------------------------------------------


Instrucciones de deploy
.......................

ATENCION: en el presente Sprint se implementaron cambios en la BD. Antes de realizar el deploy del sistema,
confirme que no haya campañas pausadas o en ejecución. El proceso de migración de la BD dejará las campañas y
templates en un estado inconsistente y no deberian ser utilizadas ni recicladas.

.. code::

    $ ssh deployer@192.168.99.224
    $ ./deploy.sh sprint12-fixes <INVENTARIO>

Para crear usuarios, es necesario loguearse en el servidor con el usuario `ftsender`
y ejecutar `/home/ftsender/deploy/bin/manage.sh create_ftsender_user`:

.. code::

    $ host> ssh ftsender@server-or-ip
    $ server> /home/ftsender/deploy/bin/manage.sh create_ftsender_user


Migraciones de datos
....................

.. code::

	* commit 2bd71d0b3bc2bfe20cf5412286f1e6250994a067
	  A     fts_web/migrations/0025_auto__add_audiodecampana.py

	* commit c930e22320a35f0cd6ee62b35bf9400d1091b531
	  A     fts_web/migrations/0026_auto__del_field_campana_audio_asterisk__del_field_campana_audio_origin.py

	* commit cdbcc8dcc90e30b322e1968279eef639e1e37bfe
	  A     fts_web/migrations/0027_auto__chg_field_audiodecampana_tts.py

	* commit edfdfa32cc2532cb44b678ad82e305a102faf03d
	  A     fts_web/migrations/0028_auto__add_unique_audiodecampana_orden_campana.py

	* commit 1bda83dd61b86dc71bec31c372247626c4b87c71
	  A     fts_web/migrations/0029_auto__add_field_audiodecampana_audio_descripcion.py


TTS / Multiples TTS
..............................................

* FTS-310 - UI: Alta de campaña (funcionalidad avanzada)
* FTS-311 - UI: Alta de campaña: volver a permitir modificación de BD
* FTS-306 - Campos fecha/hora: Daemon: obtener metadatos de BD
* FTS-307 - Campos fecha/hora: Generador de dialplan
* FTS-325 - Template de Campaña: agregar BDC de referencia
* FTS-326 - Multiples TTS: mejoras en generador de Dialplan

Diferidos para próximo Sprint
.............................

* FTS-297 - Soporte para multiples sistemas de TTS (requiere FTS-326)
