#!/bin/bash

#
# Shell script para facilitar el deploy de la aplicación desde un servidor de deploy
#
# Autor: Andres Felipe Macias
# Colaborador:  Federico Peker
#
ANSIBLE=`which ansible`
arg1=$1
PIP=`which pip`
current_directory=`pwd`
TMP_ANSIBLE='/var/tmp/ansible-ics'
ANSIBLE_VERSION_DESIRED='2.9.2'
ANSIBLE_VERSION_INSTALLED="`~/.local/bin/ansible --version |head -1| awk -F ' ' '{print $2}'`"
export ANSIBLE_CONFIG=$TMP_ANSIBLE

Help() {
USAGE="
       Deploy de FTS-ICS
       Opciones a ingresar: \n
            -h: ayuda \n
            -r: rama a deployar (ej: -r develop) \n
            -i: instala ansible, transfiere llaves ssh a maquina a deployar, ingresar fqdn, formato de grabaciones, pass de OML \n
            -t: ingresar ip, opcion de SO, tags de ansible (TAREAS A EJECUTAR y no ejecutar) \n
       EJEMPLOS: \n
       - ./deploy.sh -r develop -t all -> deployará la rama develop, ejecutara todas las tareas \n
       - ./deploy.sh -r release-0.4 -i -t kamailio,nginx,kamailio-cert -> deploya la rama release-0.4, pide datos del server, ejecuta las tareas de instalación de kamailio y de nginx  exceptuando la creacion de certificados (tiene que estar separado por coma) \n
       - ./deploy.sh -r release-0.4 -i -t asterisk,,kamailio-cert -> igual al anterior, solamente ejecutará tareas de instalación de asterisk exceptuando la creacion de certificados \n
       \n "
       echo -e $USAGE
       exit 1
}

Rama() {
    
    git clone  --branch $OMLDIALER_BRANCH https://gitlab.com/omnileads/omnileads-dialer.git
    cd omnileads-dialer
    branch_name="`git branch | grep \* | cut -d '' -f2`"
    echo "Pasando al deploy de OmniAPP $branch_name"
    set -e
    echo ""
    echo "Se iniciará deploy:"
    echo ""
    echo "      Version: $branch_name"
    #echo "   Inventario: $INVENTORY"
    echo ""

    ################### Build.sh #####################

    #set -e
    #cd $(dirname $0)
    TMP=/var/tmp/ftsender-build
    if [ -e $TMP ] ; then
        rm -rf $TMP
    fi
    mkdir -p $TMP/app
    echo "Usando directorio temporal: $TMP/ftsender..."
    echo "Creando bundle usando git-archive..."
    git archive --format=tar $(git rev-parse HEAD) | tar x -f - -C $TMP/app

    echo "Eliminando archivos innecesarios..."
    rm -rf $TMP/app/fts_tests
    rm -rf $TMP/app/docs
    rm -rf $TMP/app/deploy
    rm -rf $TMP/app/build
    rm -rf $TMP/app/run_coverage*
    rm -rf $TMP/app/run_sphinx.sh

    mkdir -p $TMP/appsms
    echo "Usando directorio temporal: $TMP/appsms..."

    mkdir -p $TMP/apidinstar
    echo "Usando directorio temporal: $TMP/apidinstar..."

    echo "Descargando api dinstar en directorio temporal"
    tar -xzf $current_directory/../aplicacionsms/dinstar.tar.gz -C $TMP/apidinstar

    echo "Obteniendo datos de version..."
    branch_name=$(git symbolic-ref -q HEAD)
    branch_name=${branch_name##refs/heads/}
    branch_name=${branch_name:-HEAD}

    commit="$(git rev-parse HEAD)"
    author="$(id -un)@$(hostname)"

    echo "Creando archivo de version | Branch: $branch_name | Commit: $commit | Autor: $author"
    cat > $TMP/app/fts_web/version.py <<EOF

#
# Archivo autogenerado
#

FTSENDER_BRANCH="${branch_name}"
FTSENDER_COMMIT="${commit}"
FTSENDER_BUILD_DATE="$(env LC_ALL=C LC_TIME=C date)"
FTSENDER_AUTHOR="${author}"

if __name__ == '__main__':
    print FTSENDER_COMMIT

EOF

    echo "Validando version.py - Commit:"
    python $TMP/app/fts_web/version.py

    # ----------
    export DO_CHECKS="${DO_CHECKS:-no}"
}

Preliminar() {

    echo "Bienvenido al asistente de instalación de ICS"
    echo ""
    echo "Detecting if Ansible $ANSIBLE_VERSION_DESIRED is installed"
    if [ -z "$ANSIBLE" ] || [ "$ANSIBLE_VERSION_INSTALLED" != "$ANSIBLE_VERSION_DESIRED" ]; then
      echo "Ansible $ANSIBLE_VERSION_DESIRED is not installed, installing it"
      echo ""
      $PIP install "ansible==$ANSIBLE_VERSION_DESIRED" --user
      ANSIBLE="`find ~/.local -name ansible |grep \"/bin/ansible\" |head -1 2> /dev/null`"
    else
      echo "Ansible $ANSIBLE_VERSION_DESIRED is already installed"
    fi

}

#IngresarIP(){
#}

Tag() {
    if [ "$arg1" == "--install" ] || [ "$arg1" == "-i" ]; then
      tag="all"
    elif [ "$arg1" == "--upgrade" ] || [ "$arg1" == "-u" ]; then
      tag="postinstall"
    fi
      IFS=$'\n'
      servers=( $(cat $current_directory/inventory | grep ansible_user=) )
      for i in ${servers[@]}; do
        if [[ ! $i = \#* ]]; then
          if [[ $i != *"ansible_connection=local"* ]]; then
            ip="`echo $i |awk -F \" \" '{print $1}'`"
            ssh_port="`echo $i |grep ansible_ssh_port |awk -F " " '{print $2}'|awk -F "=" '{print $2}'`"
            echo "Transifiendo llave publica a usuario ftsender de Centos"
            ssh-copy-id -p $ssh_port -i ~/.ssh/id_rsa.pub ftsender@$ip
          fi
        fi
      done
    if [ ! -z $SSH_KEY ];then
      extra_args="--private-key $SSH_KEY"
    fi
    ${ANSIBLE}-playbook $TMP_ANSIBLE/playbook.yml --extra-vars "BUILD_DIR=$TMP/app BUILD_DIR_SMS=$TMP/appsms BUILD_API_DINSTAR=$TMP/apidinstar" --tags "$tag" -K $extra_args
    ResultadoAnsible=`echo $?`

    if [ ${ResultadoAnsible} -ne 0 ];then
      echo "Falló la ejecucion de Ansible, favor volver a correr el script"
      exit 0
    fi
}

for i in "$@"
do
  case $i in
    --key=*)
      SSH_KEY="${i#*=}"
      shift
    ;;
    --upgrade|-u|--install|-i)
      shift
    ;;
    *)
      echo $arg1
      Help
    ;;
  esac
done
Preliminar
Rama
Tag
